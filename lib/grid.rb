class Grid
  attr_accessor :grid, :n

  def initialize(n)
    self.n = n
    self.grid = []
    self.populate_grid
  end

  def populate_grid
    #TODO: I thing good idea is to move this as initial for creating the array.:w
    self.grid = []
    0.upto(n - 1) do |i|
      row = []
      0.upto(n - 1) do |j|
        row << ('A'..'Z').to_a[rand_number]
      end
      self.grid << row
    end
  end

  def get_all_strings
    #TODO: I have the feeling that this could be implemented better. Now I will leave it, works as I excepted
    all_strings = grid.map(&:join)
    0.upto(n - 1) do |i|
      string = ''
      0.upto(n - 1) do |j|
        string += grid[j][i]
      end
      all_strings << string
    end
    all_strings << (0..n-1).collect{ |i| grid[i][i] }.join
    all_strings << (0..n-1).collect{ |i| grid[0 + i][(n-1) - i] }.join
    @all_strings ||= all_strings
  end

  def include?(str)
    !!get_all_strings.find{ |e| e.include? str } or
    # Simple trick to not double the all_strings
    !!get_all_strings.find{ |e| e.include? str.reverse }
  end

  #TODO: missing test for this, need to figured out how to test it
  def rand_number
    rand(26)
  end
end

