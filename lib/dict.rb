class Dict
  attr_accessor :data

  def initialize
    #TODO: I know that array is not a perfect way to store such big dict, and probably I would consider to store it in trie.
    #Now I what to implement it completle as fas as I can.
    self.data = []
  end

  def load_from_file(path_to_file)
    File.open(path_to_file, 'r').each_line do |line|
      word = line.split(' ').first
      self.data << word unless word.nil?
    end
  end
end
