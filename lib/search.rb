class Search
  attr_accessor :dict, :grid

  def initialize(n, path_to_file)
    self.dict = Dict.new
    self.dict.load_from_file(path_to_file)
    self.grid = Grid.new(n)
  end

  def find_words
    dict.data.find_all{|word| grid.include?(word) }
  end
end
