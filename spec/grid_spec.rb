require 'spec_helper'

describe Grid do
  describe '.initialize' do
    let(:grid) { Grid.new(2) }
    before { Grid.any_instance.should_receive(:populate_grid) }
    it { expect(grid.grid).to eql [] }
  end

  describe '#populate_grid' do
    before { allow(grid).to receive(:rand_number).and_return(1) }
    let(:grid) { Grid.new(2) }

    it 'should populate gird with random letters' do
      grid.populate_grid
      expect(grid.grid).to eql [['B', 'B'], ['B', 'B']]
    end
  end

  describe '#get_all_strings' do
    let(:grid) { Grid.new(3) }
    before { grid.grid = [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']] }

    it 'shuld return all strings' do
      expect(grid.get_all_strings).to eql [
        'ABC',
        'DEF',
        'GHI',
        'ADG',
        'BEH',
        'CFI',
        'AEI',
        'CEG'
      ]
    end
  end

  describe '#include?' do
    let(:grid) { Grid.new(3) }
    before { grid.grid = [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']] }

    it { expect(grid.include?('AEI')).to be_true }
    it { expect(grid.include?('IEA')).to be_true }
    it { expect(grid.include?('FOO')).to be_false }
  end
end
