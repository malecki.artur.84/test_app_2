require 'spec_helper'

describe Search do
  let(:dict) { double :dict, data: ['FOO', 'BAR', 'BAZ'] }
  let(:grid) { double :grid }

  it 'should find words in array' do
    expect(Dict).to receive(:new).and_return(dict)
    expect(Grid).to receive(:new).and_return(grid)
    expect(dict).to receive(:load_from_file).with('/path/to/dict.txt')
    expect(grid).to receive(:include?).with('FOO').and_return(true)
    expect(grid).to receive(:include?).with('BAR').and_return(true)
    expect(grid).to receive(:include?).with('BAZ').and_return(false)

    expect(Search.new(5, '/path/to/dict.txt').find_words).to eql ['FOO', 'BAR']
  end
end
