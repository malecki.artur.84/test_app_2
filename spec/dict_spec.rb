require 'spec_helper'

describe Dict do
  describe '#load_from_file' do
    let(:dict) { Dict.new }
    it 'should load data from file' do
      dict.load_from_file('./spec/fixtures/dict.txt')
      expect(dict.data).to eql ['AM', 'AN', 'AR']
    end
  end
end
